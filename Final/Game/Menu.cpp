#include "Menu.h"
#include "GameLoop.h"
#include <iostream>
Menu::Menu()
{
	buffer.loadFromFile("select.wav");
	sound.setBuffer(buffer);
}

Menu::~Menu()
{
}

void Menu::Run(RenderWindow *window)
{
	int opcion = 0;
	int winOrLose = 0;
	bool game = true;
	Texture texMenu;
	Texture texSelector;
	Texture texFondo;
	GameLoop *gameLoop = new GameLoop();
	Keyboard keyboard;
	Sprite fondo;
	Sprite menu;
	Sprite selector;
	Color color;
	color.Black;
	texMenu.loadFromFile("start.png");
	texSelector.loadFromFile("llave.png");
	texFondo.loadFromFile("fondo.png");
	menu.setTexture(texMenu);
	menu.setPosition(400, 300);
	menu.setOrigin(170/2, 138/2);
	fondo.setTexture(texFondo);
	fondo.setPosition(0, 0);
	selector.setTexture(texSelector);
	selector.setPosition(335, 342); //335 342 primero 415 342 segundo
	selector.setOrigin(9, 24);
	selector.setColor(color);
	selector.setScale(0.8f, 0.5f);
	

	while (window->isOpen() && game == true)
	{
		Event event;
		window->draw(fondo);
		window->draw(menu);
		window->draw(selector);

		if (keyboard.isKeyPressed(keyboard.D)
			|| keyboard.isKeyPressed(keyboard.Right)
			&& opcion != 1)
		{
			if (opcion == 0)
				sound.play();
			opcion = 1;
			selector.setPosition(410, 342);
		}
		if (keyboard.isKeyPressed(keyboard.A)
			|| keyboard.isKeyPressed(keyboard.Left)
			&& opcion != 0)
		{
			if(opcion == 1)
				sound.play();
			opcion = 0;
			selector.setPosition(335, 342);
		}
		if (keyboard.isKeyPressed(keyboard.Space)
			|| keyboard.isKeyPressed(keyboard.Enter))
		{
			sound.play();
			if (opcion == 0)
				winOrLose = gameLoop->Run(window);
			else
				game = false;
		}

		if (winOrLose == 1)
			texMenu.loadFromFile("startWin.png");
		
		if (winOrLose == 2)
			texMenu.loadFromFile("startLose.png");
		

		while (window->pollEvent(event))
		{
			if (event.type == Event::Closed)
				window->close();
		}

		window->display();
		window->clear();
	}
	delete gameLoop;
}