#ifndef MENU_H
#define MENU_H
#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>
#include <SFML\Window\Keyboard.hpp>
using namespace sf;
using namespace std;
class Menu
{
private:
	Sound sound;
	SoundBuffer buffer;
public:
	Menu();
	~Menu();
	void Run(RenderWindow *window);
};
#endif