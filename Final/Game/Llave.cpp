#include "Llave.h"

Llave::Llave()
{
	textura.loadFromFile("llave.png");
	buffer.loadFromFile("pickkey.wav");
	sound.setBuffer(buffer);
	sprite.setTexture(textura);
	sprite.setPosition(400, 300);
	sprite.setScale(1.0f, 1.0f);
}

Llave::Llave(int x, int y)
{
	textura.loadFromFile("llave.png");
	buffer.loadFromFile("pickkey.wav");
	sound.setBuffer(buffer);
	sprite.setTexture(textura);
	sprite.setPosition(x, y);
	sprite.setScale(1.0f, 1.0f);
}

Llave::~Llave()
{

}

Sprite Llave::GetSprite()
{
	return sprite;
}

void Llave::Colision(Sprite player)
{
	if (sprite.getGlobalBounds().intersects(player.getGlobalBounds())) {
		sound.play();
		sprite.setScale(0, 0);
		sprite.setPosition(1000, 1000);
	}
}

void Llave::Spawn()
{
	sprite.setPosition(200, 300);
}