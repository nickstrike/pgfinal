#ifndef PUERTA_H
#define PUERTA_H
#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>
#include "Player.h"
#include <iostream>
using namespace sf;
class Puerta
{
private:
	Texture textura;
	Sprite sprite;
	Sound sound;
	SoundBuffer buffer;
public:
	Puerta();
	Puerta(int x, int y);
	~Puerta();
	Sprite GetSprite();
	bool Colision(Sprite player, bool llave);
};
#endif