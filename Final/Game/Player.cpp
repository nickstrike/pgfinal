#include "Player.h"
#include <iostream>
using namespace std;

Player::Player()
{
	textura.loadFromFile("spriteUnarmed.png");	
	buffer.loadFromFile("hit.wav");
	sound.setBuffer(buffer);
	sprite.setTexture(textura);
	sprite.setPosition(0, 0);
	sprite.setOrigin(20, 16);
	sprite.setScale(2.f, 2.f);
}

Player::Player(int x, int y)
{
	textura.loadFromFile("spriteUnarmed.png");
	
	buffer.loadFromFile("hit.wav");
	sound.setBuffer(buffer);
	sprite.setTexture(textura);
	sprite.setPosition(x, y);
	sprite.setOrigin(20, 16);
	sprite.setScale(2.f, 2.f);
}

Player::~Player()
{
	sprite.setScale(0, 0);
	alive = false;
}

Sprite Player::GetPos()
{
	return sprite;
}

void Player::Input(float elapsed)
{
	cooldown -= elapsed;
	cout << cooldown << endl;	

	if (alive == true)
	{
		if (!keyboard.isKeyPressed(keyboard.Space))
		{
			if (keyboard.isKeyPressed(keyboard.A) || keyboard.isKeyPressed(keyboard.Left))
			{
				//sprite.move(-200 * elapsed, 0);
				if(vH >= -200)
					vH -= 1;
				sprite.setScale(-2.f, 2.f);
				dir = 3;				
			}

			if (keyboard.isKeyPressed(keyboard.D) || keyboard.isKeyPressed(keyboard.Right))
			{
				//sprite.move(200 * elapsed, 0);
				if (vH <= 200)
					vH += 1;
				sprite.setScale(2.f, 2.f);
				dir = 1;				
			}

			if (keyboard.isKeyPressed(keyboard.W) || keyboard.isKeyPressed(keyboard.Up))
			{
				//sprite.move(0, -200 * elapsed);
				if (vV >= -200)
					vV -= 1;
				dir = 0;				
			}

			if (keyboard.isKeyPressed(keyboard.S) || keyboard.isKeyPressed(keyboard.Down))
			{
				//sprite.move(0, 200 * elapsed);
				if (vV <= 200)
					vV += 1;
				dir = 2;				
			}		
		}
		else if (keyboard.isKeyPressed(keyboard.Space)) //dash
		{
			if (cooldown <= 0)
			{								
				if (dir == 0)
					vV -= 400; //sprite.move(0, -200 * elapsed);
				if (dir == 1)
					vH += 400; //sprite.move(200 * elapsed, 0);
				if (dir == 2)
					vV += 400; //sprite.move(0, 200 * elapsed);
				if (dir == 3)
					vH -= 400; //sprite.move(-200 * elapsed, 0);
				
				cooldown = 3;
			}
		}

		if (armed == true)
		{
			textura.loadFromFile("sprite.png");
			sprite.setTexture(textura);
			sprite.setOrigin(20, 16);
		}
	}
}

void Player::Mover(float elapsed)
{
	sprite.move(vH * elapsed, vV * elapsed);

	if (vH > 0)
		vH -= 0.3;

	if (vH < 0)
		vH += 0.3;

	if (vV > 0)
		vV -= 0.3;
	
	if (vV < 0)
		vV += 0.3;
}

void Player::Colision(Sprite enemigo, Sprite armamento, Sprite llave)
{
	if (armed == false) {
		if (sprite.getGlobalBounds().intersects(enemigo.getGlobalBounds()))
		{
			sound.play();
			alive = false;
		}
	}

	if (sprite.getGlobalBounds().intersects(armamento.getGlobalBounds()))
	{		
		armed = true;
		cout << "ARMED" << endl;
	}

	if (sprite.getGlobalBounds().intersects(llave.getGlobalBounds()))
	{
		out = true;
		cout << "Ready to get out of here!" << endl;
	}
	if (sprite.getPosition().x >= 800)
		sprite.setPosition(800, sprite.getPosition().y);
	if (sprite.getPosition().x <= 0)
		sprite.setPosition(0, sprite.getPosition().y);
	if (sprite.getPosition().y >= 600)
		sprite.setPosition(sprite.getPosition().x, 600);
	if (sprite.getPosition().y <= 0)
		sprite.setPosition(sprite.getPosition().x, 0);
}

bool Player::GetArmed()
{
	return armed;
}

bool Player::GetOut()
{
	return out;
}

bool Player::GetAlive()
{
	return alive;
}