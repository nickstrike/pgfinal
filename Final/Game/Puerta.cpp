#include "Puerta.h"



Puerta::Puerta()
{
	textura.loadFromFile("door.png");
	buffer.loadFromFile("door.wav");
	sound.setBuffer(buffer);
	sprite.setTexture(textura);
	sprite.setPosition(1, 300);
	sprite.setOrigin(0, 48);
	sprite.setScale(1.0f, 1.0f);
}

Puerta::Puerta(int x, int y)
{
	textura.loadFromFile("door.png");
	buffer.loadFromFile("door.wav");
	sound.setBuffer(buffer);
	sprite.setTexture(textura);
	sprite.setPosition(x, y);
	sprite.setOrigin(0, 48);
	sprite.setScale(1.0f, 1.0f);
}


Puerta::~Puerta()
{
}

Sprite Puerta::GetSprite()
{
	return sprite;
}


bool Puerta::Colision(Sprite player, bool llave)
{
	if (llave == true)
	{
		if (sprite.getGlobalBounds().intersects(player.getGlobalBounds()))
		{
			sound.play();
			return llave;
		}
	}
}
