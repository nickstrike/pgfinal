#ifndef PLAYER_H
#define PLAYER_H

#include <SFML\Graphics.hpp>
#include <SFML\Window\Keyboard.hpp>
#include <SFML\Audio.hpp>

using namespace sf;

class Player
{
private:
	int dir = 1;
	float cooldown = 0.5;
	float timer = 0;
	bool alive = true;
	bool out = false;
	bool armed = false;
	float vH = 0;
	float vV = 0;
	Sound sound;
	SoundBuffer buffer;
	Texture textura;
	Keyboard keyboard;
	Sprite sprite;
public:
	Player();
	Player(int x, int y);
	~Player();
	Sprite GetPos();
	void Input(float elapsed);
	void Mover(float elapsed);
	void Colision(Sprite enemigo, Sprite armamento, Sprite llave);
	bool GetArmed();
	bool GetOut();
	bool GetAlive();
};
#endif PLAYER_H