#ifndef LLAVE_H
#define LLAVE_H
#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>
#include "Player.h"
#include <iostream>
using namespace sf;
class Llave
{
private:
	Texture textura;
	Sprite sprite;
	Sound sound;
	SoundBuffer buffer;

public:
	Llave();
	Llave(int x, int y);
	~Llave();
	Sprite GetSprite();
	void Spawn();
	void Colision(Sprite player);
};

#endif
