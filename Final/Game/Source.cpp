#include <iostream>
#include <SFML/Graphics.hpp>
#include "GameLoop.h"
#include "Menu.h"

using namespace std;

int main()
{
	RenderWindow *window = new RenderWindow(VideoMode(800, 600), "Trapped Goblin");


	Menu *menu = new Menu();
	menu->Run(window);

	delete window;
	delete menu;
}