#include <SFML\Graphics.hpp>
#include <SFML\Window\Keyboard.hpp>
#include <SFML\Audio.hpp>
#include "Player.h"
#include "GameLoop.h"
#include "Enemigo.h"
#include "Armamento.h"
#include "Puerta.h"
#include "Llave.h"
#include <iostream>

using namespace sf;
using namespace std;

GameLoop::GameLoop()
{
}

GameLoop::~GameLoop()
{
}

int GameLoop::Run(RenderWindow *window)
{
	int num = 0;
	bool game = true;
	Sound hit;
	SoundBuffer sbHit;
	sbHit.loadFromFile("hit.wav");
	hit.setBuffer(sbHit);
	Texture texFondo;
	Sprite fondo;
	texFondo.loadFromFile("fondo.png");
	fondo.setTexture(texFondo);
	fondo.setPosition(0, 0);
	Armamento *armamento = new Armamento(750, 300); 
	Puerta *puerta = new Puerta();
	Llave *llave = new Llave(1000, 1000);
	Player *player = new Player(20, 300); 
	Enemigo *enemigo1 = new Enemigo(4, 400, 300); 
	Enemigo *enemigo2 = new Enemigo(3, 400, 300); 
	Enemigo *enemigo3 = new Enemigo(0, 700, 300); 
	Enemigo *enemigo4 = new Enemigo(2, 400, 50); 
	Clock clock;
	Time elapsed;	


	while (window->isOpen() && game == true)
	{
		elapsed = clock.restart();
		Event event;
		

		window->draw(fondo);
		window->draw(puerta->GetSprite());
		window->draw(armamento->GetSprite());
		window->draw(player->GetPos());
		window->draw(enemigo1->GetSprite());
		window->draw(enemigo2->GetSprite());
		window->draw(enemigo3->GetSprite());
		window->draw(enemigo4->GetSprite());
		window->draw(llave->GetSprite());

		player->Input(elapsed.asSeconds());
		player->Mover(elapsed.asSeconds());
		enemigo1->ChasePlayer(player->GetPos(), player->GetArmed(), elapsed.asSeconds());
		enemigo3->ChasePlayer(player->GetPos(), player->GetArmed(), elapsed.asSeconds());
		enemigo2->Jump(elapsed.asSeconds());
		enemigo4->Jump(elapsed.asSeconds());

		enemigo1->Colision(player->GetPos(), player->GetArmed());
		enemigo2->Colision(player->GetPos(), player->GetArmed());
		enemigo3->Colision(player->GetPos(), player->GetArmed());
		enemigo4->Colision(player->GetPos(), player->GetArmed());

		player->Colision(enemigo1->GetSprite(), armamento->GetSprite(), llave->GetSprite());
		player->Colision(enemigo2->GetSprite(), armamento->GetSprite(), llave->GetSprite());
		player->Colision(enemigo3->GetSprite(), armamento->GetSprite(), llave->GetSprite());
		player->Colision(enemigo4->GetSprite(), armamento->GetSprite(), llave->GetSprite());
		armamento->Colision(player->GetPos());
		llave->Colision(player->GetPos());

		if (enemigo1->GetAlive() == false &&
			enemigo2->GetAlive() == false &&
			enemigo3->GetAlive() == false &&
			enemigo4->GetAlive() == false)
			llave->Spawn();
							
		if (player->GetAlive() == false)
		{
			return 2;
			game = false;
		}
		if (puerta->Colision(player->GetPos(), player->GetOut()) == true)
		{
			return 1;
			game = false;
		}
		
		while (window->pollEvent(event));
		{
			if (event.type == Event::Closed)
				window->close();
		}

		window->display();
		window->clear(); 
	}

	delete player;
	delete enemigo1;
	delete enemigo2;
	delete enemigo3;
	delete enemigo4;
	delete llave;
	delete puerta;
	delete armamento;

}